import Mock from 'mockjs'

const users = [];
for (let i = 0; i < 10; i++) {
  users.push(Mock.mock({
    userId: '0010' + i,
    loginName: Mock.Random.name(),
    userName: Mock.Random.name(),
    deptName: '',
    email: '',
    phonenumber: ''
  }))
}

export {
  users
}
